# A small collection of scripts

## Pretty ROOT

Do you like plots generated by CERN's statistic framework [ROOT](https://root.cern/)? Me neither. TCanvas::SaveAs allows you to save your plot as TIKZ picture; however, the translation to LaTex code works poorly and a lot of modifications have to be done by hand in order to obtain code that can be compiled, e.g., with pdflatex. The script rootTex2Tex is supposed to provide an easy tool to convert LaTex code generated by ROOT to a file that can be compiled with pdflatex. This is only a very basic script! You have to add further lines according to your needs.

<p align="center">
  <img src="https://gitlab.com/csauer/scripts/raw/master/doc/fjet_m_pt350_450.png?raw=true">
</p>

Oh no, that's just awful! Let's give this plot a little makeover by calling the script root/rootTex2Tex

```bash
./root/rootTex2Tex name_of_tex_file_from_root.tex
```

After that, the same file is compiled with pdflatex

```bash
pdflatex name_of_tex_file_from_root.tex
```

And this is the result.

<p align="center">
  <img src="https://gitlab.com/csauer/scripts/raw/master/doc/fjet_m_pt350_450_fTex.png?raw=true">
</p>

That's beautiful :'). Now, all fonts in your text and the plots are consistent.
